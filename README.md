# Alpha Beta Checkers Agent

An alpha-beta checkers-playing agent that takes the current game state and makes a legal move based on a heuristic function that evaluates candidate states.

### Usage Instructions

To change the max depth, use the static final variable MAX_DEPTH in AlphaBetaSearch.java. MAX_DEPTH of 10 will take 5-10 seconds to compute the best move, while depths of 14 or more may take 2+ minutes.


## Performance Report

### Search Depth

#### Proficiency
Increasing the maximum depth of the alpha beta search significantly increased performance. At depths of less than 5, defeating the agent is trivial, but at higher depths the difficulty increases. Even at depth 14, I still often am able to defeat the agent, but I also lose to it fairly often.

#### Latency
Increasing the max depth leads to higher latency, of course. At depths of 9 or less, the AI responds almost instantly. At 10, the early game decisions are made instantly, but later in the game the latency increases. I suspect this is because the presence of kings increases the branching factor. At a depth of around 14, the agent begins to take a long time to choose a move. At this depth, it may take 2 minutes or more in the late game.


### Evaluation Function

#### Proficiency

##### Basic piece count
I tested out many different evaluation functions, many of which had very little effect on the performance of the agent. The most basic was simply a count of the pieces, where kings are worth 2 points and normal pieces are worth 1. The function calculates the difference between the number of black pieces and the number of red.

##### Average distance form becoming king
The next function incorporated how close the normal pieces were to becoming kings, since pieces close to the enemy back row are more threatening. The difference between the black and red distance scores was calculated and added to the value of the previous eval function. These values were normalized and then weighted by order of importance (pieces count is more important than distance).

##### Piece count ratio
After that I added a ratio of the black piece count to the red piece count. This metric was intended to make the agent more likely to sacrifice pieces if winning and less likely if losing. This was added to the others and weighted as least important.

##### Depth discovered
The next evaluation function incorporated the depth at which the state was discovered. The idea behind this was that if the agent found a way to win in less moves it would take preference. This value was added to the others and weighted as least important.

##### Randomness
The last piece of the eval function that I added was a random number. This is to cause a slight amount of randomness in choice between equally valuable moves. This was once again added to the others and weighted as least important.


#### Latency
I was able to improve performance of the evaluation function by storing the number of each type of piece in the state object, as well as Booleans denoting if the state was a win or loss. By storing these, I negated the need for multiple for loops within the evaluation function, allowing me to reach higher search depths in reasonable amounts of time.
