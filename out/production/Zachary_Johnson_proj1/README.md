# Alpha-Beta Pruning Checkers Agent

An alpha-beta checkers-playing agent that takes the current game state and makes a legal move based on a heuristic function that evaluates candidate states.

## Usage Instructions

To change the max depth, use the static final variable MAX_DEPTH in AlphaBetaSearch.java. MAX_DEPTH of 10 will take 5-10 seconds to compute the best move, while depths of 14 or more may take 2+ minutes.
