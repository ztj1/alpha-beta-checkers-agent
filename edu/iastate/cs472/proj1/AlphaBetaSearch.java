package edu.iastate.cs472.proj1;

/**
 * Minimax checkers agent that utilizes alpha-beta pruning to increase efficiency
 *
 * @author Zachary Johnson
 */
public class AlphaBetaSearch {
    private CheckersData board;

    // An instance of this class will be created in the Checkers.Board
    // It would be better to keep the default constructor.

    public void setCheckersData(CheckersData board) {
        this.board = board;
    }

    /**
     * An integer representing the tree depth at which the agent will stop searching and return an
     * evaluation of the current state.
     */
    private static final int MAX_DEPTH = 10;

    private double maxValue(CheckersData state, double alpha, double beta, int depth) {
        // numCalls++;
        if (isTerminal(state)) {
            return evaluate(state, depth);
        }
        double bestValue = Double.NEGATIVE_INFINITY;

        for (CheckersMove move : state.getLegalMoves(CheckersData.BLACK)) {

            CheckersData temp = copy(state);
            temp.makeMove(move);

            if (depth < MAX_DEPTH) {

                double value;
                // This accounts for multi-jumps. Adding this statement made a huge increase in performance.
                if (move.isJump() && temp.getLegalJumpsFrom(CheckersData.BLACK, move.toRow, move.toCol) != null) {
                    value = maxValue(temp, alpha, beta, depth + 1);
                } else {
                    value = minValue(temp, alpha, beta, depth + 1);
                }

                bestValue = Math.max(value, bestValue);

            } else {
                bestValue = Math.max(evaluate(temp, depth), bestValue);
            }
            alpha = Math.max(alpha, bestValue);

            if (beta <= alpha) {
                break;
            }
        }
        return bestValue;
    }

    private double minValue(CheckersData state, double alpha, double beta, int depth) {
        if (isTerminal(state)) {
            return evaluate(state, depth);
        }
        double bestValue = Double.POSITIVE_INFINITY;

        for (CheckersMove move : state.getLegalMoves(CheckersData.RED)) {

            CheckersData temp = copy(state);

            temp.makeMove(move);
            if (depth < MAX_DEPTH) {

                double value;
                // This accounts for multi-jumps. Adding this statement made a huge increase in performance.
                if (move.isJump() && temp.getLegalJumpsFrom(CheckersData.RED, move.toRow, move.toCol) != null) {
                    value = minValue(temp, alpha, beta, depth+1);
                } else {
                    value = maxValue(temp, alpha, beta, depth + 1);
                }
                bestValue = Math.min(value, bestValue);
            } else {
                bestValue = Math.min(evaluate(temp, depth), bestValue);
            }
            beta = Math.min(beta, bestValue);

            if (beta >= alpha) {
                break;
            }
        }
        return bestValue;
    }

    private double evaluate(CheckersData state, int depth) {

        double scoreDiff = 5*state.blackKings + 3*state.blackMen - 5*state.redKings - 3*state.redMen;
        double totalScore = 5*state.blackKings + 3*state.blackMen + 5*state.redKings + 3*state.redMen;
        double scoreProp = scoreDiff/totalScore;
        double scoreRatio = ((double)(5*state.blackKings + 3*state.blackMen)) / ((double)(5*state.redKings + 3*state.redMen + 1));
        scoreRatio = scoreRatio / totalScore;

        double kingAvgDistDiff = (state.redDist / (state.redMen + 1)) - (state.blackDist / (state.blackMen + 1));
        double kingDistRatio = kingAvgDistDiff / 7.0;

        double depthScaled = depth/(double)MAX_DEPTH;

        return 1000000*scoreProp + 100000*scoreRatio + 10000*kingDistRatio - 10*depthScaled + Math.random();
    }

    /**
     *  You need to implement the Alpha-Beta pruning algorithm here to
     * find the best move at current stage.
     * The input parameter legalMoves contains all the possible moves.
     * It contains four integers:  fromRow, fromCol, toRow, toCol
     * which represents a move from (fromRow, fromCol) to (toRow, toCol).
     * It also provides a utility method `isJump` to see whether this
     * move is a jump or a simple move.
     *
     * @param legalMoves All the legal moves for the agent at current step.
     */
    public CheckersMove makeMove(CheckersMove[] legalMoves) {
        // The checker board state can be obtained from this.board,
        // which is a int 2D array. The numbers in the `board` are
        // defined as
        // 0 - empty square,
        // 1 - red man
        // 2 - red king
        // 3 - black man
        // 4 - black king
        System.out.println(board);
        System.out.println();

        CheckersMove result = null;
        double resultValue = Double.NEGATIVE_INFINITY;

        // Todo: return the move for the current state
        for (CheckersMove move : legalMoves) {

            CheckersData temp = copy(this.board);

            temp.makeMove(move);

            double value = minValue(temp, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0);
            if (value > resultValue) {
                result = move;
                resultValue = value;
            }
        }

        CheckersData temp = copy(this.board);
        temp.makeMove(result);

        return result;
    }

    private static boolean isTerminal(CheckersData state) {
        return state.win || state.lose;
    }

    private CheckersData copy(CheckersData state) {

        int[][] tempBoard = new int[8][8];

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                tempBoard[i][j] = state.board[i][j];
            }
        }

        return new CheckersData(tempBoard, state.redKings, state.redMen,
                state.blackKings, state.blackMen,
                state.redDist, state.blackDist,
                state.win, state.lose);
    }
}
